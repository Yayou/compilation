open Ast
open Error

(* Informations données par l'environnement de typage. *)
type type_annot =
  | TA_var of typ
  | TA_fun of typ * (typ list)
  | TA_struc of (ident*field_typ) list

(* Type de l'environnement de typage. *)
module Env = Map.Make(String)
type  tenv = type_annot Env.t

(* Environnement vide. *)
let empty_env = Env.empty
(* Environnement de base, à compléter pour tenir compte d'éventuelles fonctions
   ou variables primitives, par exemple [print_int] et [print_newline] si
   vous en avez fait des fonctions ordinaires (extension "uniformisation" du
   TP précédent). *)
let base_env = empty_env

(* Ajout à un nœud de l'information de type. *)
let upd_type ne ty = ne.typ <- ty

(* Fonction à appeler pour rapporter des erreurs de types. *)	
let type_error l t1 t2 = error (Type_error(t1, t2)) l

let not_implemented() = failwith "Not implemented"

(* On cree trois fonctions "utilitaires". Elles prennent toutes en parametre une position loc pour l'affichage des erreurs *)

(* get_fields prend en parametre l'environnement de typage, et l'id d'une structure, et renvoie la liste des champs de la structure *)
let get_fields t_env id loc =
  try
    match Env.find id t_env with
      | TA_struc fl -> fl
      | TA_fun _    -> error (Function_identifier id)  loc
      | TA_var _    -> error (Unknown_identifier id)   loc
  with Not_found    -> error (Unknown_identifier id)   loc

(* get_struc prend en parametre l'environnement des champs, et l'id d'un champ, et renvoie le nom de la structure a laquelle appartient ce champ *)
let get_struc f_env f_id loc =
  try
    Env.find f_id f_env
  with Not_found -> error (Unknown_field_name f_id) loc

(* get_field prend en parametre le nom d'un champ et une liste de champs de la forme (string, field_typ), et renvoie le type du champ de la liste ayant le nom indique *)
let rec get_field f_name loc = function
  | [] -> error (Unknown_field_name f_name) loc
  | (f_id, f_typ)::_ when f_id = f_name -> f_typ
  | _::rest -> get_field f_name loc rest


let rec check_types l ty1 ty2 = 
  match ty1, ty2 with
  | Tunit, Tunit | Tbool, Tbool | Tint, Tint | Tnone, Tnone -> ()
  | Tident t1, Tident t2 when t1 = t2 -> ()
  | Tref t1, Tref t2 -> check_types l t1 t2
  | Tnone, Toption _ | Toption _, Tnone -> ()
  | Toption t1, Toption t2 -> check_types l t1 t2
  | _, _ -> type_error l ty1 ty2
    
let rec type_expr t_env f_env ne =
  match ne.expr with
    | Econst Cunit     ->
      (* Met à jour le type du nœud. *)
      upd_type ne Tunit

    | Econst (Cint i)  -> upd_type ne Tint
    | Econst (Cbool b) -> upd_type ne Tbool

    | Econst (Cnone)   -> upd_type ne Tnone

    | Eident id -> begin
      try
	match Env.find id t_env with
	  | TA_var ty  -> upd_type ne ty
	  | TA_fun _   -> error (Function_identifier id) ne.pos
	  | TA_struc _ -> error (Structure_identifier id) ne.pos
      with Not_found   -> error (Unknown_identifier id)  ne.pos
    end
      
    | Eunop (op, e) ->
      type_expr t_env f_env e;
      let ty = begin
	match op with
	| Unot   -> check_types e.pos Tbool e.typ; Tbool
	| Uminus -> check_types e.pos Tint e.typ;  Tint
	| Uref   -> Tref e.typ
	| Usome  -> Toption e.typ
      end in
      upd_type ne ty

    | Ebinop (op, e1, e2) ->
      type_expr t_env f_env e1;
      type_expr t_env f_env e2;
      let ty = begin
	match op with
	  | Beq | Bneq ->
	    check_types e2.pos e1.typ e2.typ;
	    Tbool
	  | Blt | Ble | Bgt | Bge ->
	    check_types e1.pos Tint   e1.typ;
	    check_types e2.pos Tint   e2.typ;
	    Tbool
	  | Badd | Bsub | Bmul | Bdiv ->
	    check_types e1.pos Tint   e1.typ;
	    check_types e2.pos Tint   e2.typ;
	    Tint
	  | Band | Bor ->
	    check_types e1.pos Tbool  e1.typ;
	    check_types e2.pos Tbool  e2.typ;
	    Tbool
      end in
      upd_type ne ty

    | Eif (e1, e2, e3) ->
      type_expr t_env f_env e1;
      type_expr t_env f_env e2;
      type_expr t_env f_env e3;
      let ty =
	check_types e1.pos Tbool  e1.typ;
	check_types e2.pos e2.typ e3.typ;
	e2.typ
      in
      upd_type ne ty

    | Eletin (id, e1, e2) ->
      type_expr t_env f_env e1;
      type_expr (Env.add id (TA_var e1.typ) t_env) f_env e2;
      upd_type ne e2.typ

    (* References *)
    (* on verifie que lorsque l'on essaye d'acceder a une reference, c'en est bien une *)
    | Egetref (e) ->
       type_expr t_env f_env e;
       let ty =
	 match e.typ with
	 | Tref t -> t
	 | _ -> failwith "expression is not a reference"
       in
       upd_type ne ty

    (* on verifie que lorsque l'on essaye de modifier une reference, non seulement c'en est une, mais en plus elle est de type compatible avec celui de la nouvelle valeur*)
    | Esetref (e1, e2) ->
       type_expr t_env f_env e1;
       type_expr t_env f_env e2;
       begin
	 match e1.typ with
	 | Tref t -> check_types ne.pos t e2.typ
	 | _ -> failwith "expression is not a reference"
       end;
       upd_type ne Tunit

		
    (* Options *)
    (* on verifie que c'est bien une option *)
    | Eletopt (id, e1, e2) ->
       type_expr t_env f_env e1;
       let ty =
	 match e1.typ with
	 | Toption t -> t
	 | _ -> failwith "expression is not an option"
       in
       let new_env = Env.add id (TA_var ty) t_env in
       type_expr new_env f_env e2;
       upd_type ne e2.typ
       

    (* Structures *)

    | Estruct (f_list) ->
       (* On commence par chercher l'id du premier field *)
       let id_fst =
	 match f_list with
	 | (id, _)::_ -> id
	 | _ -> failwith "no fields"
       in
       (* On recupere la structure correspondante *)
       let struc = get_struc f_env id_fst ne.pos in
       (* On recupere la liste des champs de la structure, de la forme (string*field_typ) list *)
       let d_list = get_fields t_env struc ne.pos in
       (* On verifie pour chaque paire (champ trouve) (champ declare) que les noms sont les memes, et que l'expression trouvee est bien du type declare *)
       let rec check_field found_list decl_list =
	 match found_list, decl_list with
	 | [], [] -> ()
	 | (id_f, ne_f)::f_list, (id_d, ftyp)::d_list ->
	   if id_f <> id_d then
	     error (Unknown_field_name id_f) ne_f.pos
	   else
	     type_expr t_env f_env ne_f;
	   let typ_d =
	     match ftyp with
	       | Fmutable t   -> t
	       | Fimmutable t -> t
	   in
	   check_types ne_f.pos typ_d ne_f.typ;
	   check_field f_list d_list
	 | _ -> failwith "wrong number of fields"
       in check_field f_list d_list;
       upd_type ne (Tident struc)
	    
    | Eget (e, id) ->
      type_expr t_env f_env e;
      (* on recupere le nom de la structure *)
      let struc =
	match e.typ with
	  | Tident id -> id
	  | _ -> error (Not_a_struct_type e.typ) ne.pos
      in
      let fields = get_fields t_env struc e.pos in  (* on recup la liste des champs *)
      let f_typ = get_field id e.pos fields in      (* on recup le type du champ *)
      (* on update avec le type du champ (qu'il soit mutable ou non) *)
      begin
	match f_typ with
	  | Fmutable t   -> upd_type ne t
	  | Fimmutable t -> upd_type ne t
      end

    | Eset (ne1, id, ne2) ->
      type_expr t_env f_env ne1;
      (* on recupere le nom de la structure *)
      let t = 
	match ne1.typ with
	  | Tident id -> id
	  | _ -> error (Not_a_struct_type ne1.typ) ne1.pos
      in
      let fields = get_fields t_env t ne1.pos in   (* on recup la liste des champs *)
      let f_ty = get_field id ne.pos fields in     (* on recup le type du champ *)
      (* si le champ n'est pas mutable, erreur. Sinon on recupere son type. S'il est de meme type que la nouvelle valeur, on update le type de l'expression a unit *)
      let ty =
	match f_ty with
	  | Fimmutable _ -> error (Not_mutable id) ne.pos
	  | Fmutable t   -> t
      in
      type_expr t_env f_env ne2;
      check_types ne.pos ty ne2.typ;
      upd_type ne Tunit
   
	
    | Eapp (id, args) -> begin
      let ty = try Env.find id t_env
	with Not_found -> error (Unknown_identifier id) ne.pos
      in
      match ty with
	| TA_fun (ty, ty_args) ->
	  List.iter2 (fun a ty_a ->
	    type_expr t_env f_env a;
	    check_types a.pos ty_a a.typ
	  ) args ty_args;
	  upd_type ne ty
	| _ -> error (Not_a_function id) ne.pos
    end

    | Eprint_int e ->
      type_expr t_env f_env e;
      check_types e.pos Tint e.typ;
      upd_type ne Tunit

    | Eprint_newline e ->
      (* Calcule le type de l'expression [e], et met à jour le
	 nœud représentant [e]. *)
      type_expr t_env f_env e;
      (* Vérifie que le type trouvé pour [e] est bien le type
	 attendu [Tunit]. *)
      check_types e.pos Tunit e.typ;
      (* Met à jour le type de l'expression complète. *)
      upd_type ne Tunit

(* on rajoute un environnement pour les champs, associant a chaque champ le nom de la structure associée *)

let type_prog p =
  (* On utilise une fonction auxiliaire, qui type une instruction et
     renvoie l'environnement de typage éventuellement mis à jour. *)
  let rec type_instr_list t_env f_env instr_list =
    match instr_list with
      | [] -> t_env
	
      | Icompute e :: il ->
	type_expr t_env f_env e;
	check_types e.pos Tunit e.typ;
	type_instr_list t_env f_env il
	  
      | Ilet (id, ty, e) :: il -> 
	type_expr t_env f_env e;
	check_types e.pos ty e.typ;
	type_instr_list (Env.add id (TA_var e.typ) t_env) f_env il
	  
      | Ifun (rec_flag, id, args, ty, e) :: il ->
	let nt_env = List.fold_left
	  (fun nt_env (a, ty_a) -> Env.add a (TA_var ty_a) nt_env)
	  t_env args
	in
	let nt_env =
	  if rec_flag
	  then Env.add id (TA_fun (ty, List.map snd args)) nt_env 
	  else nt_env
	in
	type_expr nt_env f_env e;
	check_types e.pos ty e.typ;
	type_instr_list
	  (Env.add id (TA_fun (ty, List.map snd args)) t_env)
	  f_env
	  il

      (* Structure *)
      | Istruct (id, f_list) :: il ->
	let rec add_fields env = function
	  | [] -> env
	  | (f_id, _)::rest -> add_fields (Env.add f_id id env) rest
	in
	(* on ajoute les champs a l'environnement des champs *)
	let nf_env = add_fields f_env f_list in
	(* on ajoute la structure a l'environnement de typage *)
	let nt_env = Env.add id (TA_struc f_list) t_env in
	type_instr_list nt_env nf_env il

  in
  type_instr_list base_env empty_env p
(* Remarque : cette fonction renvoie l'environnement de typage final.
   Ça peut servir dans l'extension "optimisation : valeurs inutiles". *)
