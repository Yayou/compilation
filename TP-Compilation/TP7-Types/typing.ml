open Ast
open Error

(* Informations données par l'environnement de typage. *)
type type_annot =
  | TA_var of typ
  | TA_fun of typ * (typ list)

(* Type de l'environnement de typage. *)
module Env = Map.Make(String)
type  tenv = type_annot Env.t

(* Environnement vide. *)
let empty_env = Env.empty
(* Environnement de base, à compléter pour tenir compte d'éventuelles fonctions
   ou variables primitives, par exemple [print_int] et [print_newline] si
   vous en avez fait des fonctions ordinaires (extension "uniformisation" du
   TP précédent). *)
let base_env = empty_env

(* Ajout à un nœud de l'information de type. *)
let upd_type ne ty = ne.typ <- ty

(* Fonction à appeler pour rapporter des erreurs de types. *)	
let type_error l t1 t2 = error (Type_error(t1, t2)) l

let not_implemented() = failwith "Not implemented"

let rec check_types l ty1 ty2 = 
  match ty1, ty2 with
    | Tunit, Tunit | Tbool, Tbool | Tint, Tint -> ()
    | _, _ -> type_error l ty1 ty2
    
let rec type_expr t_env ne =
  match ne.expr with
    | Econst Cunit ->
      (* Met à jour le type du nœud. *)
       upd_type ne Tunit
    | Econst (Cint _) ->
       upd_type ne Tint
    | Econst (Cbool _) ->
       upd_type ne Tbool

    | Eident id ->
       let tid =
	 try Env.find id t_env
	 with Not_found -> failwith "variable not in env"
       in     (* La fonction find renvoie le type. Reste a verifier que c'est bien le type d'une variable *)
       begin
	 match tid with
	 | TA_var t -> upd_type ne t
	 | TA_fun (_, _) -> failwith ("type of function for variable")
       end

    | Eunop (op, e) ->
       type_expr t_env e;
       begin
	 match op with
	 | Unot -> check_types e.pos Tbool e.typ; upd_type ne Tbool
	 | Uminus -> check_types e.pos Tint e.typ; upd_type ne Tint
       end

    | Ebinop (op, e1, e2) ->
       type_expr t_env e1;
       type_expr t_env e2;
       begin
	 match op with
	 | Blt | Ble | Bgt | Bge -> check_types e1.pos Tint e1.typ;
				    check_types e2.pos Tint e2.typ;
				    upd_type ne Tbool
	 | Badd | Bsub | Bmul | Bdiv -> check_types e1.pos Tint e1.typ;
					check_types e2.pos Tint e2.typ;
					upd_type ne Tint
	 | Band | Bor -> check_types e1.pos Tbool e1.typ;
			 check_types e2.pos Tbool e2.typ;
			 upd_type ne Tbool
	 | Beq | Bneq ->
		  (* cas particulier : on verifie que les deux expressions ont le meme type, et on update avec le type booleen *)
		  check_types ne.pos e1.typ e2.typ;
		  upd_type ne Tbool
       end

    | Eif (e_cond, e_then, e_else) ->
       type_expr t_env e_cond;
       check_types e_cond.pos Tbool e_cond.typ;
       type_expr t_env e_then;
       type_expr t_env e_else;
       (* cas particulier : on verifie que les deux expressions ont le meme type, et on update avec le type de l'une des deux (puisqu'elles ont le meme, on peut prendre la premiere) *)
       check_types ne.pos e_then.typ e_else.typ;
       upd_type ne e_then.typ

    | Eletin (id, e1, e2) ->
       type_expr t_env e1;
       let new_env = Env.add id (TA_var e1.typ) t_env in
       type_expr new_env e2;
       upd_type ne e2.typ

    | Eapp (id, ne_list) ->
       let rec type_params env = function
	 | [] -> ()
	 | ne :: r -> type_expr env ne; type_params env r
       in type_params t_env ne_list;
       let tf = 
	   try Env.find id t_env
	   with Not_found -> failwith "function not in environnement"
       in (* on recupere le type de la fonction et des ses parametres dans l'env *)
       begin
	 match tf with
	 | TA_fun (t, tl) -> let rec check_list ne_list typ_list =
			       match ne_list, typ_list with
			       | [], [] -> ()
			       | e::re, t::rt -> check_types e.pos t e.typ; check_list re rt
			       | _ -> failwith "unexpected number of arguments"
			     in
			     check_list ne_list tl; (* on verifie par rec que tous les arguments ont le bon type *)
			     upd_type ne t (* on update le type du noeud au type de la fonction *)
				      
	 | TA_var _ -> failwith "type of variable for function"
       end			     

    | Eprint_int e ->
       type_expr t_env e;
       check_types e.pos Tint e.typ;
       upd_type ne Tunit
	 
    | Eprint_newline e ->
      (* Calcule le type de l'expression [e], et met à jour le
	 nœud représentant [e]. *)
      type_expr t_env e;
      (* Vérifie que le type trouvé pour [e] est bien le type
	 attendu [Tunit]. *)
      check_types e.pos Tunit e.typ;
      (* Met à jour le type de l'expression complète. *)
      upd_type ne Tunit


let type_prog p =
  (* On utilise une fonction auxiliaire, qui type une instruction et
     renvoie l'environnement de typage éventuellement mis à jour. *)
  let rec type_instr_list t_env = function
    | [] -> t_env

    | Icompute ne :: r -> type_expr t_env ne; type_instr_list t_env r

    | Ilet (id, ty, ne) :: r -> type_expr t_env ne;
				check_types ne.pos ty ne.typ;
				let new_env = Env.add id (TA_var ty) t_env in
				type_instr_list new_env r

    | Ifun (rec_, id, param_list, typf, ne) :: r ->

       (* trois choses a faire :
          - creer la liste des types des parametres, avec un fold right pour commencer par le dernier
          - ajouter les variables a un env, ainsi que la fonction si elle est recursive, et verifier que dans cet env le type de ne c'est bien typf
          - ajouter a l'environnement la fonction avec son type et ceux de ses parametres, et typer la suite dans cet environnement
	*)

      let add_typ_list param list = snd(param)::list in
      let typ_list = List.fold_right add_typ_list param_list [] in

      let env_rec =
	if rec_ then Env.add id (TA_fun (typf, typ_list)) t_env
	else t_env
      in

      let rec env_params env = function
	| [] -> env
	| param :: r -> let id = fst(param) and tid = snd(param) in
			let n_env = env_params env r in
			Env.add id (TA_var tid) n_env
      in
      let env_ne = env_params env_rec param_list in 

      type_expr env_ne ne;
      check_types ne.pos typf ne.typ;
       
      let new_env = Env.add id (TA_fun (typf, typ_list)) t_env in
      type_instr_list new_env r

  in
  type_instr_list base_env p
       (* Remarque : cette fonction renvoie l'environnement de typage final.
	  Ça peut servir dans l'extension "optimisation : valeurs inutiles". *)
