print_int(25);;
print_newline();;


(* La totale : conditions imbriquees, avec des calculs arithmetiques, des comparaisons, et des booleens *)
if true then
	if 4 == 5 then
		print_int(1)
	else
		print_int(2)
else
	print_int(3)
;;

if 12 > 34 then
	print_int(4)
else
	if 2+3 == 5 && 3 <= 3 then
		print_int(5)
	else
		print_int(6)
;;
