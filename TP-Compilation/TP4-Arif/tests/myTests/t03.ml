print_int(123);;
print_newline();;

(* Test de la boucle while avec des condition fausse *)
while false do print_int(0) done;;
print_int(1);;

while (4 > 5) do print_int(4) done;;
print_int(2);;

while 4*5-1 != 19 do print_int(8) done;;
print_int(3);;
