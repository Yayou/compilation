print_int(42);;
print_newline();;

(* Utilisation de l'addtion et des parentheses, ainsi que d'un melange de comparaison entre un booleen et une comparaison *)
if (23/3)+4 != 1 then print_int(4) else print_int(3);;
if false || 4 == 5 then print_int(1) else print_int(2);;
