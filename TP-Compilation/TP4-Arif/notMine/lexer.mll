{

  open Lexing
  open Parser
  open Ast
  open Error

  (* Petite fonction utile pour la localisation des erreurs. *)
  let current_pos b =
    lexeme_start_p b,
    lexeme_end_p b

}



rule token = parse
  (* Le retour à la ligne est traité à part pour aider la localisation. *)
  | '\n'
      { new_line lexbuf; token lexbuf }
      
  (* Cadeau : la fonction print_newline et la constante unité. *)
  | "print_newline"
      { PRINT_NEWLINE }
  | "()"
      { CONST_UNIT }

      
  (* À compléter. Il faut gérer :
     - les blancs
     - les constantes
     - les noms "print_int" et "print_newline"
     - les symboles
     - les commentaires *)

  (* Et pour le fragment Arif il y a en plus :
     - les constantes booléennes
     - les mots-clés if, then, else, not
     - les symboles booléens et les comparaisons *)

      
  (* Les autres caractères sont des erreurs lexicales. *)
  | _
      { error (Lexical_error (lexeme lexbuf)) (current_pos lexbuf) }
  (* Fin du fichier. *)
  | eof
      { EOF }
