{

  open Lexing
  open Parser
  open Ast
  open Error

  (* Petite fonction utile pour la localisation des erreurs. *)
  let current_pos b =
    lexeme_start_p b,
    lexeme_end_p b

}

let digit = ['0'-'9']
let alpha = ['a'-'z' 'A'-'Z']

rule token = parse
  (* Le retour à la ligne est traité à part pour aider la localisation. *)
	| '\n'
      { new_line lexbuf; token lexbuf }
      
  (* Cadeau : la fonction print_newline et la constante unité. *)
	| "print_newline"
      { PRINT_NEWLINE }
	| "()"
      { CONST_UNIT }

  (* À compléter. Il faut gérer :
     - les blancs
     - les constantes
     - les noms "print_int" et "print_newline"
     - les symboles
     - les commentaires *)

	| ['\t' ' ']
		{ token lexbuf }

	| digit+ as n
		{ CONST_INT (int_of_string(n)) }
	| "("
		{ LPAREN }
	| ")"
		{ RPAREN }

	| "print_int"
		{ PRINT_INT }

	| "+"
		{ PLUS }
	| "-"
		{ MINUS }
	| "*"
		{ STAR }
	| "/"
		{ SLASH }

	| "(*"
		{ comment lexbuf; token lexbuf }	(* On gere les commentaires imbriques en entrant dans la fonction comment, puis en relancant la fonction token quand la fonction comment a fini *)

	| ";;"
		{ EOI }
      
  (* Fin du fichier. *)
  | eof
      { EOF }

  (* Les autres caractères sont des erreurs lexicales. *)
  | _
      { error (Lexical_error (lexeme lexbuf)) (current_pos lexbuf) }

and comment = parse
	| "*)"
		{ () }							(* Si on rencontre la fin d'un commentaire, on renvoie unit, et on rend de fait la main a la fonction suivante *)
	| "(*"
		{ comment lexbuf; comment lexbuf; }			(* Si un nouveau commentaire commence, on relance comment une deuxieme fois. Il faudra donc rencontrer deux fois le symbole de fin de commentaire pour finir les deux fonctions comment, et reprendre la fonction token *)
	| eof
		{ failwith "Il manque la fin d'un commentaire" }	(* Si on arrive a la fin alors qu'on est encore dans la fonction comment, c'est qu'il y a une erreur *)
	| _
		{ comment lexbuf }					(* Si on trouve un autre caractere, c'est qu'il est commente. On continue donc la fonction comment *)
