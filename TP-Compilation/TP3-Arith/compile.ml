open Ast
open Mips
open Format



(* Constantes pour la représentation des données. *)
let word_size   = 4
let data_size   = word_size

let not_implemented() = failwith "Not implemented"



(* Les fonctions [push], [peek] et [pop] sont là pour vous aider à manipuler
   la pile. *)
  
(* val push : register -> text 
  [push reg] place le contenu de [reg] au sommet de la pile.
  $sp pointe sur l'adresse de la dernière case occupée. *)
let push reg =
  sub sp sp oi word_size
  @@ sw reg areg (0, sp)

(* val peek : register -> text
  [peek reg] place la valeur en sommet de pile dans [reg] sans dépiler. *)
let peek reg =
  lw reg areg (data_size - 4, sp)

(* val pop : register -> text
  [pop reg] place la valeur en sommet de pile dans [reg] et dépile. *)
let pop reg =
  peek reg
  @@ add sp sp oi data_size

(* val replace : register -> text
   [replace reg] remplace la valeur au somment de la pile par la valeur dans [reg] sans empiler.
   Cette fonction permet, de meme que peek, de s'economiser l'addition ou la soustraction deplacant le pointeur quand on sait ce que contient le sommet de la pile, et qu'on veut le remplacer. *)
let replace reg =
  sw reg areg (0, sp)



    
(* La fonction de compilation des expressions prend en argument :
   l'expression à compiler. *)
let rec compile_expr e =
  match e.expr with

    | Econst c -> begin
      match c with
		| Cunit       -> push zero
	  
	(* Quand on a un entier, on recupere sa valeur, on la charge dans un registre, puis on met sur la pile le contenu de ce registre *)
		| Cint n	-> li a0 n @@ push a0
    end

    (* Pour l'affichage, on calcul la valeur de l'argument, puis on saute au
       fragment de code gérant l'affichage proprement dit. *)
    | Eprint_newline e ->
      let e_code = compile_expr e in
      e_code
      @@ jal "print_newline"

    (* L'affichage d'un entier se fait de facon semblable a celui d'une ligne : on compile l'expression, puis on appelle la fonction print_int *)
   | Eprint_int e ->
     let e_code = compile_expr e in
     e_code
     @@ jal "print_int"

    (* Pour Eunop, on prevoit un match sur l'operation pour preparer l'ajout d'autres operations.
       On commence par compiler l'expression, puis recuperer la commande correspondant a l'operation. On regarde ensuite l'element sur le dessus de la pile, on fait le calcul, et on remplace le dessus de la pile par le resultat. *)
   | Eunop (op, e) ->
     let e_code = compile_expr e
     and operation = begin
       match op with
       | Uminus -> sub a0 zero oreg a0		(* On fait 0 - a0, ou a0 est la valeur sur laquelle on veut faire l'operation du - unaire *)
	end
     in
     e_code
     @@ peek a0
     @@ operation
     @@ replace a0

    (* Pour Ebinop, on fait un match sur l'operation. Le reste du code est similaire a celui de Eunop, a la difference pres qu'on a deux arguments. Donc on retire le premier de la pile, puis on regarde le deuxieme, avant de le remplacer par le resultat. *)
   | Ebinop (op, e1, e2) ->
     let e_code1 = compile_expr e1
     and e_code2 = compile_expr e2
     and operation = begin
       match op with
		(* Remarque : quand on fait 1-2, c'est 2 qui est sur le sommet de la pile au moment de faire la soustraction. Donc il faut faire attention a bien prendre les registres contenant les parametres dans le bon sens *)
       | Badd -> add a0 a0 oreg a1
       | Bsub -> sub a0 a0 oreg a1
       | Bmul -> mul a0 a0 oreg a1
       | Bdiv -> div a0 a0 oreg a1
	end
     in
     e_code2
     @@ e_code1
     @@ pop a0
     @@ peek a1
     @@ operation
     @@ replace a0
	


      
(* Les instructions sont calculées l'une après l'autre. *)
let rec compile_instr_list il =
  match il with
    | []       -> nop

    (* À compléter pour le cas [Icompute] et l'itération. *)
    | Icompute e :: reste -> compile_expr e @@ compile_instr_list reste



      
(* Fragments de code pour [print_int] et [print_newline]. *)
let built_ins () =
  label "print_newline"
  @@ pop zero
  @@ li v0 11
  @@ li a0 10
  @@ syscall
  @@ push zero
  @@ jr ra

(* À compléter avec le code de [print_int]. *)
  @@ label "print_int"
  @@ pop a0		(* D'apres la spec de print_int, on retire du sommet de la pile la valeur a afficher *)
  @@ li v0 1
  @@ syscall
  @@ push zero
  @@ jr ra




    
(* La compilation du programme produit un code en trois parties :
   1/ Le code principal (label "main")
   2/ Une instruction de fin (label "end_exec")
   3/ Le code des fonctions d'affichage (built_ins_code)
*)
let compile_prog p =
  let main_code = compile_instr_list p in
  let built_ins_code = built_ins () in
  
  { text =
     comment "Code principal"
  @@ label "main"
  @@ main_code
       
  @@ comment "Fin"
  @@ label "end_exec"
  @@ li v0 10
  @@ syscall
       
  @@ comment "Primitives"
  @@ built_ins_code
  ;
    
    data = nop
  }
