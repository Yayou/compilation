print_int 1;;
print_newline();;


(* Test des conditions imbriquees *)
if (false) then
	print_int 0
else
	if (true) then
		print_int 1
	else
		print_int 2
;;
