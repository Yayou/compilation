print_int 67;;
print_newline();;


(* Combinaison de print, d'operation arithmetique, let, let in et if *)
let x = 5;;
print_int(x+1);;
if (12!=12)
	then print_int 0
else
	if (true) then let y = 4 in print_int(y+5-6/3)
	else print_int 0
;;
