type position = Lexing.position * Lexing.position

type ident = string

type const =
  | Cint of int
  | Cbool of bool
  | Cunit

type unop = Unot | Uminus
type binop =
  | Beq  | Bneq | Blt  | Ble  | Bgt  | Bge
  | Badd | Bsub | Bmul | Bdiv
  | Band | Bor

type node_expr = { expr: expr; pos: position }

and expr =
  | Econst  of const                                       (* OK *)
  | Eident  of ident                                       (* OK *)
  | Eunop   of unop        * node_expr                     (* OK *)
  | Ebinop  of binop       * node_expr   * node_expr       (* PB *)
  | Eif     of node_expr   * node_expr   * node_expr       (* OK *)
  | Eseq    of node_expr list                              (* OK *)
  | Eletin  of ident       * node_expr   * node_expr       (* OK *)
  | Eprint_int of node_expr                                (* OK *)
  | Eprint_newline of node_expr                            (* OK *)
      

type instr =
  | Icompute of node_expr                                  (* OK *)
  | Ilet     of ident * node_expr                          (* OK *)
      
type prog = instr list
