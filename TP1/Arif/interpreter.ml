open Ast
open Error

let not_implemented () = failwith "Not implemented"

(* Les expressions de notre langage calculent des valeurs qui sont soit
   des entiers, soit des booléens, soit la valeur unité qui ne représente
   rien.
*)
type value =
    Vint of int
  | Vbool of bool
  | Vunit


(* Cette déclaration nous permet de définir des environnements associant
   des valeurs à des chaînes de caractères.

   Le type de ces environnements est noté ['a Env.t], sachant que dans
   dans notre cas le paramètre ['a] sera systématiquement [value].
   Pour manipuler ces environnements, on a notamment les fonctions
   [Env.find : string -> 'a Env.t -> 'a] et
   [Env.add  : string -> 'a -> 'a Env.t -> 'a Env.t],
   ainsi que la constante [Env.empty : 'a Env.t].
   Voir la documentation de la bibliothèque Caml [Map.Make].
*)
module Env = Map.Make(String)


(* La fonction suivante prend une valeur (de type [value]) et en extrait
   le booléen qu'elle contient.
   Elle déclenche une erreur [Interpretation_error] si elle est utilisée
   sur une valeur non adaptée (comme [Vunit]). L'argument [pos] sert en cas
   d'erreur à indiquer la partie du fichier interprété qui a provoqué l'erreur.
   
   get_bool : Ast.position -> value -> bool
*)
let get_bool pos v =
  match v with
    | Vbool b -> b
    | _       -> error Interpretation_error pos

let get_int pos v =
  match v with
  | Vint i -> i
  | _      -> error Interpretation_error pos
		    
(* Voici le cœur de l'interpréteur, qu'il va falloir compléter. Cette fonction
   prend en argument un environnement associant des valeurs à des chaînes de
   caractères ainsi qu'une expression de la forme décrite dans [Ast.mli], et
   renvoie la valeur calculée.

   interpret_expr : value Env.t -> Ast.node_expr -> value
*)
let rec interpret_expr env e =
  match e.expr with
    (* Voici deux cas en exemple, à vous d'écrire les autres ! *)

    | Econst c ->
       begin
	 match c with
	 | Cint i -> Vint i
	 | Cbool b -> Vbool b
	 | Cunit -> Vunit
       end

    | Eident i -> Env.find i env

    | Eunop (Unot, e)   ->
      let b = get_bool e.pos (interpret_expr env e) in
      Vbool (not b)

    | Eunop (Uminus, e) ->
       let i = get_int e.pos (interpret_expr env e) in
       Vint (-i)

    | Ebinop ((Badd | Bsub | Bmul | Bdiv) as op, e1, e2) ->
       begin
	 match op with
	   
	 | Badd ->
	    let i1 = get_int e1.pos (interpret_expr env e1)
	    and i2 = get_int e2.pos (interpret_expr env e2)
	    in
	    Vint (i1+i2)
		 
	 | Bsub ->
	    let i1 = get_int e1.pos (interpret_expr env e1)
	    and i2 = get_int e2.pos (interpret_expr env e2)
	    in
	    Vint (i1-i2)
		 
	 | Bmul ->
	    let i1 = get_int e1.pos (interpret_expr env e1)
	    and i2 = get_int e2.pos (interpret_expr env e2)
	    in
	    Vint (i1*i2)
		 
	 | Bdiv ->
	    let i1 = get_int e1.pos (interpret_expr env e1)
	    and i2 = get_int e2.pos (interpret_expr env e2)
	    in
	    Vint (i1/i2)

	 | _ -> assert false
				
       end

    | Ebinop ((Beq | Bneq) as op, e1, e2) ->
       let e1 = interpret_expr env e1
       and e2 = interpret_expr env e2
       in
       Vbool (match op with
		Beq -> e1 = e2
	      | Bneq -> e1 <> e2
	      | _ -> assert false
	     )

    | Ebinop ((Blt | Ble | Bgt | Bge) as op, e1, e2) ->
       let i1 = get_int e1.pos (interpret_expr env e1)
       and i2 = get_int e2.pos (interpret_expr env e2)
       in
       Vbool (match op with
	      | Blt -> i1 < i2
	      | Ble -> i1 <= i2
	      | Bgt -> i1 > i2
	      | Bge -> i1 >= i2
	      | _ -> assert false
	     )
	 		  
    | Ebinop ((Band | Bor) as op, e1, e2) ->
       let b1 = get_bool e1.pos (interpret_expr env e1)
       and b2 = get_bool e2.pos (interpret_expr env e2)
       in
       Vbool (match op with
	      | Band -> b1 && b2
	      | Bor -> b1 || b2
	      | _ -> assert false
	     )

    | Eif (c,als,sinon) ->
      let cond = get_bool c.pos (interpret_expr env c)
      in
      if (cond) then
	interpret_expr env als
      else
	interpret_expr env sinon
		  
    | Eseq (liste) ->
       let rec parcours exprList =
	 match exprList with
	 | [] -> failwith "Sequence d'instructions vides"
	 | [e] -> interpret_expr env e
	 | e::liste ->
	    let _ = interpret_expr env e in
	    parcours liste
       in
       parcours liste
	    
    | Eletin (i, eI, e) ->
      let v = interpret_expr env eI
      in interpret_expr (Env.add i v env) e

    | Eprint_int e ->
       let i = get_int e.pos (interpret_expr env e) in
       print_int i;
       Vunit
	    
    | Eprint_newline e ->
      let _ = interpret_expr env e in
      print_newline ();
      Vunit


(* Enfin, la fonction principale, qu'il faut aussi compléter, et qui doit
   appliquer la fonction d'interprétation des expressions à toutes les
   instructions du programme. N'oubliez pas que les instructions peuvent
   agir sur l'environnement !

   interpret_prog : Ast.prog -> unit
*)      
let interpret_prog (p : Ast.prog) : unit =
  let rec parcours env instrList =
    match instrList with
    | []        -> print_newline()
    | (Icompute ins)::list ->
       let _ = interpret_expr env ins
       in parcours env list
    | (Ilet (i, e))::list ->
      let v = interpret_expr env e
      in parcours (Env.add i v env) list
  in
  parcours (Env.empty) p
;;
