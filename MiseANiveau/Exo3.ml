type head_elem = Title of string
	       | Style of string
;;

type body_elem =
    A of string * body_elem list   (* lien hypertexte (adresse, contenu) *)
  | B of body_elem list            (* mise en gras *)
  | P of body_elem list            (* paragraphe *)
  | Ul of body_elem list list      (* liste non enumeree *)
  | Text of string                 (* du texte simple *)
;;

type document = { head : head_elem list;
		  body : body_elem list }

let titre = Title "Ma page";;

let style = Style "a { text-decoration: underline; }";;

let p = P [Text "Un paragraphe de texte (dont un texte "; B [Text "en gras"]; Text ")"];;

let ul = Ul [[Text "Premier point"]; [Text "est un lien"; A ("http://www.google.com", [Text "Deuxième point"]); Text "est un lien"]];;

let doc = {head = [titre; style]; body = [p;ul]};;


(* version reinventee *)
let rec has_title l =
  match l with
  | [] -> false
  | Title(_)::_ -> true
  | _::r -> has_title r
;;

let test = has_title [style; titre];;

(* version simple *)
let has_title2 l = List.exists
		     (fun x -> match x with Title _ -> true | _ -> false) l;;       (* List.exists renvoie vrai si l'une des valeurs de la liste vérifie le prédicat de la fonction *)

let test = has_title2 doc.head;;


let rec valid_head l =
  match l with
  | [] -> false
  | Title(_)::r -> not(has_title r)
  | _::r -> valid_head r
;;

let test = not(has_title [titre]);;
let test = valid_head [style; titre; titre];;


let rec has_link elt =
  match elt with
  | A(_) -> true
  | Text(_) -> false
  | B content | P content -> List.exists( fun x -> has_link x) content      (*RAPPEL : on veut faire la meme chose, donc on peut tout mettre ensemble *)
  | Ul list -> List.exists (fun ll -> List.exists has_link ll) list
;;

let test = has_link (Text "test");;
let test = has_link (B [Text "test"; A ("test", [Text "test"])]);;
let test = has_link (Ul [[Text "test"]; [Text "test"; A ("test", [Text "test"])]]);;

  
let rec valid_link elt =
  match elt with
  | Text _ -> true
  | A (_,l) -> not(List.exists has_link l)
  | B contents | P contents -> List.for_all valid_link contents                        (* List.forall renvoie vrai si tous les elements de la liste verifient le predicat de la fonction *)
  | Ul l -> List.for_all (fun ll -> List.for_all valid_link ll) l
;;

let test = valid_link (Text "test");;
let test = valid_link (A ("test", [Text "test"; A ("test", [Text "test"])]));;


let rec boldify_first e =
  match e with
  | Text _ -> e
  | A (href, content) -> A(href, List.map boldify_first content)
  | P c -> P (List.map boldify_first c)
  | B c -> B (List.map boldify_first c)
  | Ul [] -> Ul []
  | Ul (e::rest) -> Ul ([B (List.map boldify_first e)] ::
			  List.map (fun l -> List.map boldify_first l) rest)
;;
  
