(* Tri par insertion*)

let rec inserer x l =
  match l with
  | [] -> x::[]
  | e::r -> if e < x then e::(inserer x r)
	    else x::l
;;

let test = inserer 3 [1;2;4;5];;

let rec ins2 lt x l =
  match l with
  | [] -> x::[]
  | e::r -> if (lt e x) then e::(inserer x r)
	    else x::l
;;

let comp x y =
  x < y
;;

let test = ins2 comp 3 [1;2;4;5];;

let rec trier lt l =
  match l with
  | [] -> []
  | e::r -> let t = trier lt r
	    in ins2 lt e t
;;

let test = trier comp [1;45;78;2;6];;


  (* Tri rapide *)

let rec partage x l =
  match l with
  | [] -> ([],[])
  | e::r -> let t = partage x r in
	    if e < x then (e::(fst t),snd t)
	    else (fst t, e::(snd t))
;;

let test = partage 3 [1;2;0;3;4;5;7;2];;

let rec quicksort l =
  match l with
  | []     -> []
  | [x]    -> l
  | [x; y] -> if x > y then [y;x] else [x;y]
  | p::r   -> let (g,d) = partage p r in
	         let g' = quicksort g
	         and d' = quicksort d in
	            g'@[p]@d'
;;

let test = quicksort [1;45;78;2;6];;
  
