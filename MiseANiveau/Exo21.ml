let rec longueur l =
  match l with
   | []     -> 0
   | _ :: r -> longueur r + 1;;


let rec applique f l =
  match l with
  | [] -> []
  | e::r -> (f e)::(applique f r);;

let f x = x+.4.0;;

let test = applique f [-1.0; -2.0; -3.0];;

let rec renverse l =
  match l with
  | [] -> []
  | e::r -> (renverse r)@(e::[]);; (* on peut ecrire [e] *)

let test = renverse [1; 2; 3];;
  
let rec trouve l k =
  match l with
  | [] -> raise Not_found
  | (x, y)::r -> if x = k then y else trouve r k;;

let test = trouve [(1,2); (2,3)] 0;;

