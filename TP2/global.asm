        .text
main:
	# Déplacement de $sp et initialisation de $gp
	sub	$sp, $sp, 8
	move	$gp, $sp
	
	# Calcul de la valeur de [x], et stockage à $gp.
	li	$a0, 2
	sw	$a0, 0($gp)
	
	# Calcul de la valeur de [y], et stockage à $gp + 4.
	# On récupère d'abord la valeur de [x].
	lw	$a0, 0($gp)
	# Multiplication
	mul	$a0, $a0, $a0
	# Stockage de [y]
	sw	$a0, 4($gp)

	# Calcul de [y + x] et affichage.
	# Plaçons [x] et [y] sur la pile et utilisons la fonction d'addition
	# sur la pile. Notez que cela ne change rien à l'accès aux variables
	# globales.
	# Lecture et empilement de [x] (toujours à $gp)
	lw	$a0, 0($gp)
	sub	$sp, $sp, 4
	sw	$a0, 0($sp)
	# Lecture et empilement de [y] (toujours à $gp + 4)
	lw	$a0, 4($gp)
	sub	$sp, $sp, 4
	sw	$a0, 0($sp)
	# Addition
	jal	stack_add
	# Affichage
	lw	$a0, 0($sp)
	jal	print_int
	# Fin
	li	$v0, 10
	syscall

# Addition de deux entiers sur la pile.
stack_add:
	# On lit les deux premiers éléments de la pile
	lw	$a0, 0($sp)
	lw	$a1, 4($sp)
	# Addition
	add	$a0, $a0, $a1
	# On replace le sommet de la pile
	add	$sp, $sp, 4
	# On enregistre le résultat
	sw	$a0, 0($sp)
	# Retour
	jr	$ra

# Affichage d'un entier se trouvant dans le registre $a0.
print_int:
	li	$v0, 1
	syscall
	jr	$ra
