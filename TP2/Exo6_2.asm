.text
main:

	# On ajoute manuellement une donnee quelconque a la tete de la pile
	li $t2, 1
	addi $sp, $sp, -4
	sw $t2, 0($sp)
	
	move $fp, $sp
	
	li $t0, 3
	addi $sp, $sp, -4
	sw $t0, 0($sp)
	
	lw $t0, 0($sp)
	add $t0, $t0, $t0
	addi $sp, $sp, -4
	sw $t0, 0($sp)
	
	