.text
main:
	# On se stocke dans $t0, $t1 et $t2 les valeurs que l'on va vouloir ajouter a la pile
	li $t0, 1
	li $t1, 2
	li $t2, 3
	
	# On les ajoute a la pile
	addi $sp, $sp, -4
	sw $t0, 0($sp)
	addi $sp, $sp, -4
	sw $t1, 0($sp)
	addi $sp, $sp, -4
	sw $t2, 0($sp)
	
	# On fait une premiere fois le fragment qui additionne les deux premiers et les stocke en tete de pile
	jal add_2
	
	# On recupere la premiere valeur de la pile, et on verifie
	lw $a0, 0($sp)
	jal print_int
	
	# On recommence
	jal add_2
	lw $a0, 0($sp)
	jal print_int
	
	# Exit
	li $v0, 10
	syscall
	
add_2:
	# On commence par recuperer les valeurs nous interessant, en les depilant
	lw $t0, 0($sp)
	addi $sp, $sp, 4
	lw $t1, 0($sp)
	addi $sp, $sp, 4
	# On additionne les registres dans lesquels les valeurs sont stockees
	add $t0, $t0, $t1
	# On empile le resultat de l'addition
	addi $sp, $sp, -4
	sw $t0, 0($sp)
	jr $ra
	
print_int:
	li $v0, 1 #On stocke 1 dans $v0. A partir de la, les syscall appeleront print_int
	#NB : la valeur a afficher est stockee dans $a0
	syscall
	jr $ra
	
print_space:
	li $v0, 11 #On stocke 11 dans $v0, pour faire un print_char
	li $a0, 32 #On stocke le code ascii de l'espace dans a0
	syscall
	jr $ra
