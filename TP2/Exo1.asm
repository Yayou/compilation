.text
main:

	li $a0, 4
	addi $a0, $a0, 6
	jal print_int
	jal print_space
	
	li $a0, 21
	li $t0, 2
	mul $a0, $a0, $t0
	jal print_int
	jal print_space
	
	li $a0, 7
	li $t0, 2
	div $a0, $a0, $t0
	addi $a0, $a0, 4
	jal print_int
	jal print_space
	
	li $a0, 10
	li $t0, 5
	div $a0, $a0, $t0
	li $t0, 6
	mul $a0, $a0, $t0
	sub $a0, $zero, $a0
	addi $a0, $a0, 3
	jal print_int
	jal print_space
	
	li $v0, 10
	syscall
	
print_int:
	li $v0, 1 #On stocke 1 dans $v0. A partir de la, les syscall appeleront print_int
	#NB : la valeur a afficher est stockee dans $a0
	syscall
	jr $ra
	
print_space:
	li $v0, 11
	li $a0, 32
	syscall
	jr $ra
