.text
main:
	li $a0, 1
	andi $a0, $a0, 0x00
	jal print_int
	jal print_space
	
	li $t0, 3
	li $t1, 4	
	beq $t0, $t1, else
	li $a0, 10
	li $a1, 2
	mul $a0, $a0, $a1
	j next
else:	li $a0, 14
next:	jal print_int
	jal print_space

	li $t0, 2
	li $t1, 3
	seq $a0, $t0, $t1
	li $t0, 2
	li $t1, 3
	mul $t1, $t0, $t1
	li $t0, 4
	sle $a1, $t0, $t1
	or $a0, $a0, $a1
	jal print_int
	jal print_space
	
	li $v0, 10
	syscall

print_int:
	li $v0, 1 #On stocke 1 dans $v0. A partir de la, les syscall appeleront print_int
	#NB : la valeur a afficher est stockee dans $a0
	syscall
	jr $ra
	
print_space:
	li $v0, 11 #On stocke 11 dans $v0, pour faire un print_char
	li $a0, 32 #On stocke le code ascii de l'espace dans a0
	syscall
	jr $ra
