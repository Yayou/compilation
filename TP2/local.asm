        .text
main:
	# Initialisation : $fp pointe sur la première case qu'occupera la pile
	sub	$fp, $sp, 4

	
	# let x = 3 in x * x
	# On va donner à [x] la première case de la pile, soit l'adresse $fp
	
	# Définition et sauvegarde de [x]
	li	$a0, 3
	sw	$a0, 0($fp)
	# Lecture de [x], calcul et affichage
	lw	$a0, 0($fp)
	mul	$a0, $a0, $a0
	jal	print_int

	
	# let x = 3 in (let y = ...) + (let z = ...)
	# On va donner à [x] la première case de la pile (adresse $fp), et [y]
	# et [z] vont tous deux utiliser la deuxième case (adresse $fp - 4).
	# Les variables locales [y] et [z] peuvent utiliser le même espace
	# car leurs portées sont disjointes.
	# On met à jour le registre de sommet de pile à chaque fois qu'une
	# variable vient prendre une nouvelle case.

	# Définition et sauvegarde de [x]
	li	$a0, 3
	sw	$a0, 0($fp)
	sub	$sp, $sp, 4
	# Définition et sauvegarde de [y] (nécessite d'abord de lire [x])
	lw	$a0, 0($fp)
	add	$a0, $a0, $a0
	sw	$a0, -4($fp)
	sub	$sp, $sp, 4
	# Multiplication de [x] et [y] (commence par les deux lectures)
	lw	$a0, 0($fp)
	lw	$a1, -4($fp)
	mul	$a0, $a0, $a1
	# On stocke le résultat sur la pile
	sub	$sp, $sp, 4
	sw	$a0, 0($sp)

	# Définition et sauvegarde de [z] (nécessite de lire [x]
	lw	$a0, 0($fp)
	add	$a0, $a0, 3
	sw	$a0, -4($fp)
	# Division de [z] par [z] (nécessite de lire [z])
	lw	$a0, -4($fp)
	div	$a0, $a0, $a0
	# On stocke le résultat sur la pile
	sub	$sp, $sp, 4
	sw	$a0, 0($sp)

	# Addition sur la pile
	jal	stack_add

	# On retire les variables locales de la pile, et on y remet à la place
	# le résultat.
	lw	$a0, 0($sp)
	add	$sp, $sp, 8
	sw	$a0, 0($sp)

	# Affichage du résultat (qui est la dernière chose qui se trouve encore
	# sur la pile)
	lw	$a0, 0($sp)
	jal	print_int

	# Fin
	li	$v0, 10
	syscall

# Addition de deux entiers sur la pile.
stack_add:
	# On lit les deux premiers éléments de la pile
	lw	$a0, 0($sp)
	lw	$a1, 4($sp)
	# Addition
	add	$a0, $a0, $a1
	# On replace le sommet de la pile
	add	$sp, $sp, 4
	# On enregistre le résultat
	sw	$a0, 0($sp)
	# Retour
	jr	$ra

# Affichage d'un entier se trouvant dans le registre $a0.
print_int:
	li	$v0, 1
	syscall
	jr	$ra
