.text
main:

	# On ajoute manuellement une donnee quelconque a la tete de la pile
	li $t2, 1
	addi $sp, $sp, -4
	sw $t2, 0($sp)

	# On stocke l'adresse actuelle de la tete de pile dans $fp
	move $fp, $sp
	# On ajoute notre x
	li $t0, 3
	addi $sp, $sp, -4
	sw $t0, 0($sp)
	lw $t0, 0($sp)
	mul $a0, $t0, $t0
	move $sp, $fp # Equivalent de addi $sp, $sp, 4
	jal print_int
	
	lw $a0, 0($sp)
	jal print_int
	
	li $v0, 10
	syscall
	
print_int:
	li $v0, 1 #On stocke 1 dans $v0. A partir de la, les syscall appeleront print_int
	#NB : la valeur a afficher est stockee dans $a0
	syscall
	jr $ra
	