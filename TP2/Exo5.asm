.text
main:
	# On empile deux fois la valeur 0 dans la pile. Ce sera notre lieu de stockage de x et y
	addi $sp, $sp, -4
	sw $zero, 0($sp)
	# On stocke l'adresse dans $gp (on veut celle du premier element stocke !)
	move $gp, $sp
	addi $sp, $sp, -4
	sw $zero, 0($sp)

	# On stocke 2 dans la case reservee pour la valeur de x	
	li $t0, 2
	sw $t0, 0($gp)
	
	# On recupere la valeur de x, on la multiplie avec elle meme, et on stocke le resulat dans la case
	# reservee pour y
	lw $t0, 0($gp)
	mul $t0, $t0, $t0
	sw $t0, -4($gp)

	# On recupere les valeurs de x et y, on les additionne, et on affiche le resultat
	lw $t0, 0($gp)	
	lw $t1, -4($gp)
	add $a0, $t0, $t1
	jal print_int

	li $v0, 10
	syscall
	
print_int:
	li $v0, 1 #On stocke 1 dans $v0. A partir de la, les syscall appeleront print_int
	#NB : la valeur a afficher est stockee dans $a0
	syscall
	jr $ra
	
print_space:
	li $v0, 11 #On stocke 11 dans $v0, pour faire un print_char
	li $a0, 32 #On stocke le code ascii de l'espace dans a0
	syscall
	jr $ra